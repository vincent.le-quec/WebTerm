var days = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
];

var months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
];

var now = new Date();

var my_time = {
    month: now.getMonth(),
    date: now.getDate(),
    day: now.getDay(),
    hour: now.getHours(),
    minute: now.getMinutes(),
    second: now.getSeconds(),
    affDate: function () {
        return days[this.day] + " " + months[this.month] + " " + this.date;
    },
    affTime: function () {
        return this.hour + ":" + this.minute + ":" + this.second;
    },
    updateTime: function () {
        var up = new Date();

        this.month = up.getMonth();
        this.date = up.getDate();
        this.day = up.getDay();
        this.hour = up.getHours();
        this.minute = up.getMinutes();
        this.second = up.getSeconds();
    }
};

function myClock() {
    my_time.updateTime();
    if (my_time.second < 10)
        my_time.second = "0" + my_time.second;
    if (my_time.minute < 10)
        my_time.minute = "0" + my_time.minute;
    document.getElementById("date").innerHTML = my_time.affDate();
    document.getElementById("time").innerHTML = my_time.affTime();
}

window.onload = function () {
    window.setInterval(myClock, 500);
};
/***-_-_-_-**/
/** useful **/
/***-_-_-_-**/

/** Nav bar **/
function my_home() {
    // $(".term").slideUp();
    my_term();
    $("#soundcloud").slideUp();
    $("#youtube").slideUp();
}

function my_browser() {
    window.open('https://www.google.com');
}

function my_term() {
    $(".term").slideToggle();
    if ($(".term").height() > 1)
        $("#icon_term").css("border-color", "#FF5500");
    else
        $("#icon_term").css("border-color", "black");
}

function my_music(str) {
    var yt = $("#youtube");
    var sc = $("#soundcloud");

    if (str.localeCompare("#soundcloud") == 0 && yt.height() > 1)
        yt.slideUp();
    else if (str.localeCompare("#youtube") == 0 && sc.height() > 1)
        sc.slideUp();
    $(str).slideToggle();
}

function my_info() {
    window.open("http://vincent.le-quec.datsite.eu/#realisations")
}