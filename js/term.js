var name = "Unknown";
    name = prompt("Please enter your name:", name);
var i = 0;
var selector = "line";
var my_prompt =
    "\t(~/Datsite/WebTerm)\n" +
    "\t" + name + "@Serv42 $  ";

function affchar(target, char) {
    $("." + selector + i).append(char);
}

var showText = function (target, message, index, interval) {
    if (index < message.length) {
        target.append(message[index++]);
        setTimeout(function () {
            showText(target, message, index, interval);
        }, interval);
    }
};

function afftxt(str) {
    $("#cmd").append(my_prompt + "<span class=\"" + selector + i + "\"></span><b></b>");
    $("." + selector + i)
        .append(str + "\n")
        .removeClass(selector);
    $("b").remove();
    i++;
};

function affprompt() {
    $("#cmd").append(my_prompt + "<span class=\"" + selector + i + "\"></span><b></b>");
};

$(document).ready(function () {
    document.getElementById("cmd").innerHTML = my_prompt + "<span class=\"line\"></span><b></b>";
    $(".line")
        .append("Hello " + name + ", it's time to start !!!\n");
    $("b").remove();

    afftxt("This project is currently in standby and allowed me to discover JavaScript.");
    afftxt("Once I have more knowledge, I would add to this website games and a real terminal like bash.");
    affprompt();
});

/****----****/
/** Tools **/
/***----***/
function close_term() {
    $(".term").slideUp();
    // reset
    $("#cmd").remove();
    $(".term").append("<pre id=\"cmd\"></pre>");
    affprompt();
}

function min_term() {
    $(".term").slideUp();
    $("#icon_term").css("border-color", "#FF5500");
}

var f_click = true;

function resize_term() {
    var tmp = $(".term");

    if (f_click) {
        if ($(window).width() <= 768)
            tmp.css({
                "position": "fixed",
                "top": "0",
                "left": "3em",
                "margin": "0",
                "padding": "0",
                "width": "100%"
            });
        else
            tmp.css({
                "position": "fixed",
                "top": "0",
                "left": "5em",
                "margin": "0",
                "padding": "0",
                "width": "100%"
            });
        f_click = false;
    } else {
        tmp.css({
            "position": "static",
            "margin": "4em auto",
            "padding": "3px 5px",
            "width": "75vw"
        });
        f_click = true;
    }
}

function test(key) {
    var touche = window.event ? key.keyCode : key.which;
    console.log(touche);
}
